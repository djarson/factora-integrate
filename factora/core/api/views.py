from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView,RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from django.db import transaction
from django.contrib.auth.models import Group
from django.forms import model_to_dict
from .tokens import create_jwt_pair_for_user
from rest_framework import generics

from rest_framework.authtoken.models import Token

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from django.views.decorators.csrf import requires_csrf_token



SECRET = "my-secret"
import datetime

import jwt


from config import settings

from django.contrib.auth import authenticate, login


from core.api.serializers import *
from core.pos.models import Category, Product, Client,User


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializers

    def list(self, request, *args, **kwargs):
        return Response({'id': 4})

class CategoryListAPIView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

    # def get_queryset(self):
    #     return self.get_serializer().Meta.model.objects.all()

    def get(self, request, *args, **kwargs):
        # print(self.request.query_params['name'])
        # print(self.request.query_params.get('name', 'William Vargas'))
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        print(self.queryset)
        # items = [i.toJSON() for i in Category.objects.all()]
        # queryset = self.get_serializer().Meta.model.objects.all()
        serializer = self.serializer_class(self.queryset.all(), many=True)
        # return self.list(request, *args, **kwargs)
        return Response(serializer.data)
    
class ClientListAPIView(generics.ListAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializers
 
    def get(self, request, *args, **kwargs):
        print(self.queryset)
        serializer = self.serializer_class(self.queryset.all(), many=True)
     
        # return Response({'data':serializer.data})

        return Response(serializer.data)
        # return Response({'data':serializer.data})

    # def post(self, request, *args, **kwargs):
    #     print(self.queryset)
    #     serializer = self.serializer_class(self.queryset.all(), many=True)
     
    #     # return Response({'data':serializer.data})

    #     return Response(serializer.data)
    
class ProductListAPIView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializers

    def post(self, request, *args, **kwargs):
        print(self.queryset)
        serializer = self.serializer_class(self.queryset.all(), many=True)
     
        # return Response({'data':serializer.data})

        return Response(serializer.data)
    
class ProductSlugListAPIView(ListAPIView):
    
    
    serializer_class = ProductSerializers

    def get(self, request, *args, **kwargs):
            try:
                # pdb.set_trace()
                id = kwargs.get('id')
                product = Product.objects.get(pk=id)
                serializer = ProductSerializers(product)
                
                return Response(serializer.data)
            
            except Product.DoesNotExist as err:
                return Response(data={
                    'success': False,
                    'message:': 'Could Not Retrieve Data',
                    'data': f"Product with ID={kwargs.get('id')} Does not Exist"
                    },
                    status=status.HTTP_400_BAD_REQUEST)

class ProductCreateAPIView(CreateAPIView):
    serializer_class = ProductSerializers

    def post(self, request, *args, **kwargs):
        print(self.request.data)
        return self.create(request, *args, **kwargs)

class CategoryCreateAPIView(CreateAPIView):
    serializer_class = CategorySerializers

    def post(self, request, *args, **kwargs):
        print(self.request.data)
        return self.create(request, *args, **kwargs)

class CategoryUpdateAPIView(UpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers

    def put(self, request, *args, **kwargs):
        # print(self.request.data)
        return self.update(request, *args, **kwargs)

class CategoryDestroyAPIView(DestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers

    def delete(self, request, *args, **kwargs):
        instance = self.queryset.get(pk=self.kwargs['pk'])
        instance.delete()
        return Response({'msg': f"Se ha eliminado correctamente el pk {self.kwargs['pk']}"})

class CategoryAPIView(APIView):

    def get(self, request, *args, **kwargs):
        print(self.request.query_params)
        return Response({'resp': False})

    def post(self, request, *args, **kwargs):
        queryset = Category.objects.all()
        # serializer = CategorySerializers(queryset, many=True)
        serializer = [i.toJSON() for i in queryset]
        return Response(serializer)

class UserCreateAPIView(CreateAPIView):
    serializer_class = UserSerializers

    def post(self, request, *args, **kwargs):
        # data = {}
        # action = request.POST['action']
        # with transaction.atomic():
                user = User()
                user.names = request.POST['names']
                user.dni = request.POST['dni']
                user.username = request.POST['username']
                
                user.create_or_update_password(user.dni)
                user.email = request.POST['email']
                user.save()
                        
                client = Client()
                client.user_id = user.id
                client.mobile = request.POST['mobile']
                client.address = request.POST['address']
                client.birthdate = request.POST['birthdate']
                user.rol = request.POST['rol']
                client.save()
                group = Group.objects.get(pk=settings.GROUPS.get('client'))
                user.groups.add(group)
                return Response({'ok': True})
            #return self.create(request, *args, **kwargs)

class LoginAPIView(APIView):

    def post(self, request, *args, **kwargs):
        response = {'resp': False, 'msg': 'El usuario no existe'}
        now = datetime.datetime.now()
        try:
            email = request.data.get('email')
            password = request.data.get('password')
            data = authenticate( password=password,email=email)
            
            if data is not None:
                payload = { 
                    "sub": data.id,
                    "nombres": data.names,
                    "username": data.username,
                    'email': data.email,
                    "rol": data.rol,
                    "exp": now + datetime.timedelta(days=7)
                    }

                token = jwt.encode(payload, SECRET, algorithm="HS256")
                print (token)
                return Response({'success': True,'data': data.toJSON(),'token': token})
            else:
                return Response(data={"message": "Invalid dataname or password"})
                # response = {'resp': True, 'data': model_to_dict(data, exclude=['last_login', 'email_reset_token', 'password', 'data_permissions', 'groups', 'date_joined'])}
        except Exception as e:
            response = {'resp': False, 'msg': str(e)}
        return Response(response)
    
class LoginClientAPIView(APIView):

    def post(self, request, *args, **kwargs):
        response = {'resp': False, 'msg': 'El usuario no existe'}
        now = datetime.datetime.now()
        if request.user is not None:
                idc = request.query_params.get('id', None)

                reg=User.objects.get(id=idc)
            
                return Response({'success': True,'data': reg.toJSON()})
        else:
            return Response(data={"message": "Invalid dataname or password"})
    

# vista para listar los carritos de compra
class CartListView(generics.ListAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

# vista para ver los detalles de un carrito de compra específico
class CartDetailView(generics.RetrieveAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    
# vista para listar los ítems del carrito
class CartItemListView(generics.ListAPIView):
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer

# vista para agregar o actualizar un ítem del carrito
class CartItemCreateUpdateView(generics.CreateAPIView, generics.UpdateAPIView):
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer

class UserCartListView(generics.ListAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

# vista para agregar un producto al carrito de compra
class AddToCartView(APIView):
    def post(self, request):
        product_id = request.data.get('product_id')
        customer_id = request.data.get('id')

        try:
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return Response({'error': 'Producto no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

        try:
            customer = Client.objects.get(id=customer_id)
        except Client.DoesNotExist:
            return Response({'error': 'Cliente no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

        cart, created = Cart.objects.get_or_create(user=customer.user)

        cart_item, created = CartItem.objects.get_or_create(
            cart=cart,
            product=product,
        )

        if not created:
            cart_item.quantity += 1
            cart_item.save()

        serializer = CartItemSerializer(cart_item)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

# vista para agregar o restar un ítem del carrito de compra
class AddRemoveCartItemView(APIView):
    def post(self, request):
        product_id = request.data.get('producto')
        customer_id = request.data.get('cliente')
        action = request.data.get('action')

        try:
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return Response({'error': 'Producto no encontrado david.'}, status=status.HTTP_404_NOT_FOUND)

        try:
            customer = Client.objects.get(id=customer_id)
        except Client.DoesNotExist:
            return Response({'error': 'Cliente no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

        cart, created = Cart.objects.get_or_create(user=customer.user)

        cart_item, created = CartItem.objects.get_or_create(
            cart=cart,
            product=product,
        )

        if action == 'add':
            cart_item.quantity += 1
            cart_item.save()
            # if cart_item.product.id==product_id:
            #     return Response({'error': 'El producto ya existe.'}, status=status.HTTP_404_NOT_FOUND)
        elif action == 'remove':
            if cart_item.quantity > 1:
                cart_item.quantity -= 1
                cart_item.save()
            else:
                cart_item.delete()

        serializer = CartItemSerializer(cart_item)

        # return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'data':serializer.data},status=status.HTTP_200_OK)

    

# vista para filtrar los ítems del carrito de compra de un cliente
class FilterCartItemsView(APIView):
    def get(self, request):
        customer_id = request.query_params.get('id')

        try:
            customer = Client.objects.get(user=customer_id)
        except Client.DoesNotExist:
            return Response({'error': 'Cliente no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

        cart_items = CartItem.objects.filter(
            cart__user=customer.user,
        ).count()

        serializer = CartItemSerializer(cart_items, many=True)

        # return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'data':serializer.data},status=status.HTTP_200_OK)
    
    

# vista para quitar un producto del carrito de compra
class RemoveFromCartView(APIView):
    def post(self, request):
        item_id = request.data.get('item_id')

        try:
            cart_item = CartItem.objects.get(id=item_id)
        except CartItem.DoesNotExist:
            return Response({'error': 'Ítem de carrito no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

        cart_item.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)



# class CustomAuthToken(ObtainAuthToken):

#     def post(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.data,
#                                            context={'request': request})
#         print ("INFORMACION",request.data)
#         serializer.is_valid(raise_exception=True)                 #  restframework
#         user = serializer.validated_data['user']
#         token, created = Token.objects.get_or_create(user=user)
#         return Response({
#             'token': token.key,
#             # 'email': user.email,
#             # 'password': user.password
#             'user': user.toJSON()
#         })
    

