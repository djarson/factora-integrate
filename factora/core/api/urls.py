from django.urls import path
from core.api.views import *
from core.api.routers import router
from rest_framework.authtoken import views

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

urlpatterns = [
    path('category/', CategoryAPIView.as_view(), name='api_category'),

    path('category/list/', CategoryListAPIView.as_view(), name='api_category_list'),
    path('category/create/', CategoryCreateAPIView.as_view(), name='api_category_create'),
    path('category/update/<int:pk>/', CategoryUpdateAPIView.as_view(), name='api_category_update'),
    path('category/delete/<int:pk>/', CategoryDestroyAPIView.as_view(), name='api_category_delete'),
    path('product/list/', ProductListAPIView.as_view(), name='api_product_list'),
    path('product/create/', ProductCreateAPIView.as_view(), name='api_product_create'),


    path('product/list/', ProductListAPIView.as_view(), name='api_product_list'),
    path('product/list/slug/<int:id>/', ProductSlugListAPIView.as_view(), name='api_product_list'),

    path('client/list/', ClientListAPIView.as_view(), name='api_client_list'),
    path('client/create/', UserCreateAPIView.as_view(), name='api_client_create'),

    # path('user/list/', UserListAPIView.as_view(), name='api_user_list'),


    # path('api-token-auth/', CustomAuthToken.as_view()),


    # path('login_admin/',LoginApi.as_view,name='api_login'),
    path('login/', LoginAPIView.as_view(), name='api_login'),

    path('create/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/token/', TokenRefreshView.as_view(), name='token_refresh'),
    path('verify/token/', TokenVerifyView.as_view(), name='token_verify'),

    

    path('obtener_cliente_guest/', LoginClientAPIView.as_view, name='api_cliente_guest'),

    path('cart/', CartItemListView.as_view(), name='cart-list'),
    path('cart/<int:pk>/', CartDetailView.as_view(), name='cart-list-i'),

    path('cart/item/', CartItemListView.as_view(), name='cart-item-list'),

    path('cart/item/<int:pk>/', CartItemCreateUpdateView.as_view(), name='cart-item-create-update'),


# solo agragar items de carrito de compras

    path('add-to-cart/', AddToCartView.as_view(), name='add-to-cart'),
# agragar o quitar items de carrito de compras
    path('cart/add/', AddRemoveCartItemView.as_view(), name='add-to-cart'),

# filtrado de items por cliente
    path('filter/cart/items/', FilterCartItemsView.as_view(), name='filter-cart-items'),


]

urlpatterns += router.urls
