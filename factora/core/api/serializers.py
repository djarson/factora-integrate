from rest_framework import serializers
from rest_framework.authtoken.models import Token
from core.pos.models import Category, Product, Client,User,Cart,CartItem



class UserSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email', 'password']

class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class ClientSerializers(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def to_representation(self, instance):
        return instance.toJSON()


class ProductSerializers(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        return instance.toJSON()

class CartSerializer(serializers.ModelSerializer):

    customer = UserSerializers(read_only=True)
    # used to represent the target of the relationship using its __unicode__ method
    # items = serializers.StringRelatedField(many=True)

    class Meta:
        model = Cart
        fields = '__all__'

class CartItemSerializer(serializers.ModelSerializer):

    cart = CartSerializer(read_only=True)
    product = ProductSerializers(read_only=True)

    class Meta:
        model = CartItem
        fields = '__all__'



