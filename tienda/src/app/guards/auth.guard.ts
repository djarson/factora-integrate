import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ClienteService } from "src/app/services/cliente.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private _clienteService: ClienteService,
    private _router: Router,
  ) {
    
  }

  canActivate(): any{
    if (!this._clienteService.isAuthenticated()) { //aqui ya no paso ningu rol por q el cliente es un solo tipo de usuario
      this._router.navigate(['/login']);
      return false;
    }
    return true;
  }
  
}
