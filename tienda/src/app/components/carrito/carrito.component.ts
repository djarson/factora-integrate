import { Component, ElementRef,OnInit, ViewChild } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { GLOBAL } from 'src/app/services/GLOBAL';
import { io } from "socket.io-client";
import { GuestService } from 'src/app/services/guest.service';

declare var Cleave: any;
declare var StickySidebar:any;
declare var iziToast: any;
declare var paypal: any;

//esta inntrface es para paypal
interface HtmlInputEvent extends Event{
  target : HTMLInputElement & EventTarget;
} 

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  @ViewChild('paypalButton', { static: true }) paypalElement!: ElementRef;

  public idcliente: any;
  public token: any;
  public carrito_arr : Array<any> = [];
  public url:any;
  public subtotal = 0 ;
  public total_pagar = 0;
  public socket = io('http://localhost:4201')
  public direccion_principal : any = {};
  public envios :Array<any>= [];
  public precio_envio = "0";

  public venta: any = {};
  public dventa: any = [];
 


  constructor( 
    private _clienteService: ClienteService,
    private _guestService: GuestService) { 
    
    this.idcliente = localStorage.getItem('id')
    this.venta.cliente=this.idcliente
    this.token=localStorage.getItem('token')
    this.url = GLOBAL.url;
    
    
    this._guestService.get_Envios().subscribe(
      response => {
        this.envios=response;
      }
    )

  }

  ngOnInit(): void {

    this.init_data();
    
    setTimeout(() => {
      new Cleave('#cc-number', {
        creditCard: true,
        onCreditCardTypeChanged: function (type: any) {
          
        }
    });
    new Cleave('#cc-exp-date', {
      date: true,
      datePattern: ['m', 'y']
    });
    var sidebar = new StickySidebar('.sidebar-sticky', {topSpacing: 20});//codigo para sidebar  flotante Order Total
    });

    this.get_direccion_principal();
    this.venta.direccion = this.direccion_principal;
// codigo para iincializar paypal
    paypal.Buttons({
      style: {
          layout: 'horizontal'
      },
      createOrder: (data: any,actions: any)=>{
  
          return actions.order.create({
            purchase_units : [{
              description : 'Nombre del pago',
              amount : {
                currency_code : 'USD',
                value: 999
              },
            }]
          });
        
      },
      onApprove : async (data: any,actions: any)=>{
        const order = await actions.order.capture();
        console.log(order);
        this.venta.transaccion = order.purchase_units[0].payments.captures[0].id;
        console.log(this.dventa)

        this.venta.detalles = this.dventa;
        this._clienteService.registro_compra_cliente(this.venta,this.token).subscribe(
          response => {
            console.log(response);
          }
        );
      },
      onError : () =>{
       
      },
      onCancel: function (data: any,actions: any) {
        
      }
    }).render(this.paypalElement.nativeElement);

    

  }

  
  init_data() {
    this._clienteService.obtener_carrito_cliente(this.idcliente, this.token).subscribe(
      response => {
        this.carrito_arr = response.data;
        // console.log(this.carrito_arr);
        
        this.carrito_arr.forEach(element => {
          this.dventa.push({
            producto: element.producto._id,
            subtotal: element.producto.precio,
            variedad: element.variedad,
            cantidad: element.cantidad,
            cliente:localStorage.getItem('id')
            })
        })


        this.calcular_carrito();
        this.calcular_total('Envio gratis');
      }
    )
  }

  get_direccion_principal() {
    this._clienteService.obtener_direccion_principal_cliente(localStorage.getItem('id'),this.token).subscribe(
      response=>{
        if (response.data == undefined) {//para validar si el cliente no tiene direccion
          this.direccion_principal = undefined;
        } else {
          this.direccion_principal = response.data;
          this.venta.direccion=this.direccion_principal._id;
        }
      })
  }


  calcular_carrito() {
    this.subtotal=0;
    this.carrito_arr.forEach(element => {
      this.subtotal = this.subtotal + parseInt(element.producto.precio);
    });
    this.total_pagar = this.subtotal;
  }

  eliminar_item(id: any){
    this._clienteService.eliminar_carrito_cliente(id,this.token).subscribe(
      response=>{
        iziToast.show({
          title:'SUCCESS',
          titleColor:'#1DC74C',
          color: '#FFF',
          class: 'text-success',
          position:'topRight',
          message:'se elimino el producto correctamente'
        });
        this.socket.emit('delete-carrito',{data:response.data});
        this.init_data();
        
      }
    )
   }

  calcular_total(envio_titulo: any){
     this.total_pagar = parseInt(this.subtotal.toString()) + parseInt(this.precio_envio);
     this.venta.subtotal = this.total_pagar;
     this.venta.envio_precio = parseInt(this.precio_envio)
     this.venta.envio_titulo=envio_titulo
     
    
  }

}
